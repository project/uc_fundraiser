<?php
/**
 * @file
 * Admin interface functions.
 *
 * Overview page to view all fundraiser nodes.
 */

/**
 * Administration menu item to view fundraiser campaigns.
 */
function uc_fundraiser_overview() {
  // First get a list of all of the nodes for the table.
  $sql = "SELECT nid FROM {uc_fundraiser}";
  $result = db_query($sql);
  $nids = array();
  while ($nid = db_result($result)) {
    $nids[] = $nid;
  }

  // With all the nids available, load the node for each of them and put
  // the desired informaiton in the table.
  $rows = array();
  foreach ($nids as $nid) {

    // Get both campaing and product nodes.
    $fundraiser_node = node_load($nid);
    $product_node = node_load($fundraiser_node->pid);

    // Set up table information.
    $row['title'] = l($fundraiser_node->title, 'node/' . $fundraiser_node->nid);
    $row['product_title'] = l($product_node->title, 'node/' . $product_node->nid);
    $row['given'] = $fundraiser_node->given;
    $row['needed'] = $fundraiser_node->needed;

    // Add edit links.
    $links = array(
      l(t('Edit Fundraiser'), 'node/' . $fundraiser_node->nid . '/edit'),
      l(t('Edit Product'), 'node/' . $product_node->nid . '/edit'),
    );
    $row['links'] = implode(' | ', $links);

    // Add the new row to the output matrix.
    $rows[] = $row;
  }

  // Create the header.
  $header = array(
    t('Title'),
    t('Product'),
    t('Given'),
    t('Needed'),
    t('Links'),
  );
  $output = theme('table', $header, $rows);

  return $output;
}
