Ubercart Fundraiser creates a node type to track product sales towards a goal.

For example. If you are raising funds to build wells in in africa and your
current fundraising campaign calls for 500 people to donate $50, you can use
a campaign to track the number of $50 donation's towards this goal. Or if
you're trying to sell 100 handbags made by refugees to help feed them you can
create a campaign for that too.

How to:

1. Put the module in your contributed module directory
  /sites/all/modules/
  Remmber to unzip it if you havn't already
  
2. Enable the module
  Go to /admin/build/modules
  Check the box next to "Ubercart Fundraiser"
  Click save at the bottom of the page.
  
3. Create a product to represent what your fundraiser campaign is raising like
  $50 Donation towards a well for a poor vilage in Africa. 
  Treat this as an ordinary product. It can be a shippable item or
  just a donation.

4. Create a fundraiser
   Go to /node/add/uc_fundraiser
   Enter the number of items you want.
   Enter the number of items already given (or leave at default zero if just
    started)
   Enter the name of the product, the field will automatically provide options
     for you from products in your system.
   Save the Fundraiser.

5. Customize your node view, create views, etc.

To view all Fundraisers and their status/progress 
visit: admin/content/fundraiser
