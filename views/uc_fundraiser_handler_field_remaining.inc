<?php

/**
 * @file
 * Views handler: Fundraiser Campaign remaining.
 */

/**
 * Return a formatted price value to display in the View.
 */
class uc_fundraiser_handler_field_remaining extends views_handler_field_numeric {

  /**
   * Overide the query to include the additional fields but not the primary
   * field.
   */
  function query() {
    $this->ensure_my_table();
    $this->add_additional_fields();
  }


  /**
   *  Run the calculations neccesary to render the values.
   */
  function render($values) {

    // Calculate the value using the fundraiser function.
    $value = $values->{$this->aliases['uc_fundraiser_needed']} - $values->{$this->aliases['uc_fundraiser_given']};

    // Check to see if hiding should happen before adding prefix and suffix.
    if ($this->options['hide_empty'] && empty($value) && ($value !== 0 || $this->options['empty_zero'])) {
      return '';
    }
    return check_plain($this->options['prefix'] . $value . $this->options['suffix']);
  }
}
