<?php

/**
 * @file
 * Views 2 hooks and callback registries.
 */

/**
 * Implements hook_views_data().
 */
function uc_fundraiser_views_data() {

  $data['uc_fundraiser']['table']['group'] = t('Fundraiser');

  $data['uc_fundraiser']['table']['join'] = array(
    'node' => array(
      'left_field' => 'nid',
      'field' => 'nid',
    ),
  );

  $data['uc_fundraiser']['progress'] = array(
    'title' => t('Progress Percent'),
    'help' => t('The percentage of the fundraiser campaign that is completed.'),
    'field' => array(
      'additional fields' => array(
        'uc_fundraiser_given' => array(
          'table' => 'uc_fundraiser',
          'field' => 'given',
        ),
        'uc_fundraiser_needed' => array(
          'table' => 'uc_fundraiser',
          'field' => 'needed',
        ),
      ),
      'handler' => 'uc_fundraiser_handler_field_progress',
      'click sortable' => TRUE,
    ),
    'sort' => array(
      'handler' => 'views_handler_sort',
    ),
    'filter' => array(
      'handler' => 'views_handler_filter_numeric',
    ),
    'argument' => array(
      'handler' => 'views_handler_argument_numeric',
    ),
  );

  $data['uc_fundraiser']['remaining'] = array(
    'title' => t('Remaining Donations Needed'),
    'help' => t('The percentage of the fundraiser campaign that is completed.'),
    'field' => array(
      'additional fields' => array(
        'uc_fundraiser_given' => array(
          'table' => 'uc_fundraiser',
          'field' => 'given',
        ),
        'uc_fundraiser_needed' => array(
          'table' => 'uc_fundraiser',
          'field' => 'needed',
        ),
      ),
      'handler' => 'uc_fundraiser_handler_field_remaining',
      'click sortable' => TRUE,
    ),
    'sort' => array(
      'handler' => 'views_handler_sort',
    ),
    'filter' => array(
      'handler' => 'views_handler_filter_numeric',
    ),
    'argument' => array(
      'handler' => 'views_handler_argument_numeric',
    ),
  );

  // Add a relationship to the product to display the products info.
  $data['uc_fundraiser']['pid'] = array(
    'title' => t('Product'),
    'help' => t('The product this fundraiser campaign references.'),
    'relationship' => array(
      'handler' => 'views_handler_relationship',
      'base' => 'node',
      'base field' => 'nid',
      'label' => t('product'),
    ),
  );

  $data['uc_fundraiser']['needed'] = array(
    'title' => t('Needed'),
    'help' => t('The number of products needed for the fundraiser campaign.'),
    'field' => array(
      'handler' => 'views_handler_field_numeric',
      'click sortable' => TRUE,
    ),
    'sort' => array(
      'handler' => 'views_handler_sort',
    ),
    'filter' => array(
      'handler' => 'views_handler_filter_numeric',
    ),
    'argument' => array(
      'handler' => 'views_handler_argument_numeric',
    ),
  );

  $data['uc_fundraiser']['given'] = array(
    'title' => t('Given'),
    'help' => t('The products given to the fundraiser campaign so far.'),
    'field' => array(
      'handler' => 'views_handler_field_numeric',
      'click sortable' => TRUE,
    ),
    'sort' => array(
      'handler' => 'views_handler_sort',
    ),
    'filter' => array(
      'handler' => 'views_handler_filter_numeric',
    ),
    'argument' => array(
      'handler' => 'views_handler_argument_numeric',
    ),
  );

  $data['uc_fundraiser']['active'] = array(
    'title' => t('Active'),
    'help' => t('True if the fundraiser campaign is active.'),
    'field' => array(
      'handler' => 'views_handler_field_boolean',
    ),
    'filter' => array(
      'handler' => 'views_handler_filter_boolean',
    ),
    'argument' => array(
      'handler' => 'views_handler_argument_boolean',
    ),
  );

  return $data;
}


/**
 * Implements hook_views_handlers().
 */
function uc_fundraiser_views_handlers() {
  return array(
    'info' => array(
      'path' => drupal_get_path('module', 'uc_fundraiser') . '/views',
    ),
    'handlers' => array(
      'uc_fundraiser_handler_field_progress' => array(
        'parent' => 'views_handler_field_numeric',
      ),
      'uc_fundraiser_handler_field_remaining' => array(
        'parent' => 'views_handler_field_numeric',
      ),
    ),
  );
}
