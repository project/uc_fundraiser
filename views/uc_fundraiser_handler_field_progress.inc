<?php

/**
 * @file
 * Views handler: Fundraiser Campaign Progress Bar Handler.
 */

/**
 * Return a formatted price value to display in the View.
 */
class uc_fundraiser_handler_field_progress extends views_handler_field_numeric {

  /**
   * Add the percent bar option to the options form.
   */
  function option_definition() {

    // Include default numeric field options.
    $options = parent::option_definition();

    // Add format options os users may choose percent bar.
    $options['format'] = array('default' => 'percent_bar');
    return $options;
  }

  /**
   *  Define the format option on the options form.
   */
  function options_form(&$form, &$form_state) {

    // Include the default numeric field options.
    parent::options_form($form, $form_state);

    // Grab the values already set so they can be used as default values.
    $options = $this->options;

    // Create format form element.
    $form['format'] = array(
      '#title' => t('Format'),
      '#type' => 'radios',
      '#options' => array(
        'percent_bar' => t('Percent Bar'),
        'numeric' => t('Numeric'),
      ),
      '#default_value' => $options['format'],
      '#weight' => 1,
    );
  }

  /**
   *  Overide the query to include the additional fields but not the primary
   *  field.
   */
  function query() {
    $this->ensure_my_table();
    $this->add_additional_fields();
  }


  /**
   * Run the calculations neccesary to render the values.
   */
  function render($values) {

    // Calculate the value using the fundraiser function.
    $value = uc_fundraiser_calculate_percent($values->{$this->aliases['uc_fundraiser_given']}, $values->{$this->aliases['uc_fundraiser_needed']});

    // If the value needs to be outputed as numeric just output the raw number.
    // If the user wants a percent symbol or something like that, they can
    // use the suffix.
    if ($this->options['format'] == 'numeric') {

      // Check to see if hiding should happen before adding prefix and suffix.
      if ($this->options['hide_empty'] && empty($value) && ($value !== 0 || $this->options['empty_zero'])) {
        return '';
      }
      return check_plain($this->options['prefix'] . $value . $this->options['suffix']);
    }

    // If the format is percent bar output with the percent bar theme.
    if ($this->options['format'] == 'percent_bar') {
      return theme('uc_fundraiser_percent_bar', $value);
    }
  }
}
